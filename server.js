var express = require("express");
var engine = require('express-dot-engine');
var request = require('request');
var lodash = require('lodash');
var moment = require('moment');
var querystring = require('querystring');
var session = require('express-session');
var async = require('async');


// settings
var host = process.env.IP;
var port = process.env.PORT;
var poinoutApp = '575c50bea1f87203004f6285';

var app = express();
app.use(session({ secret: poinoutApp, cookie: { maxAge: 86400000 }, resave: false, saveUninitialized: false}));

engine.settings.dot = {
  evaluate:    /\{\{([\s\S]+?)\}\}/g,
  interpolate: /\{\{=([\s\S]+?)\}\}/g,
  encode:      /\{\{!([\s\S]+?)\}\}/g,
  use:         /\{\{#([\s\S]+?)\}\}/g,
  define:      /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,
  conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,
  iterate:     /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
  varname: 'layout, partial, locals, it',
  strip: false,
  append: true,
  selfcontained: false
};

var api = require('./api.js');
api.load(app);

var loadData = function(cb){
  console.time('LOADING DATA');
  request('https://poinout.herokuapp.com/api/json/' + poinoutApp, function (error, response, body) {
    if(error || response.statusCode !== 200){
      var msg = 'Poinout data can not be loaded. Check if poinoutApp "' + poinoutApp + '" id is correct';
      return cb && cb({ok: false, msg: error || msg});
    }
    app.data = JSON.parse(body);
    app.data.year = new Date().getFullYear();
    app.data.server = {poinoutApp: poinoutApp};
    // extra data modifications for VIEWS
    app.data.server.tags = [];
    app.data.flickr && app.data.flickr.forEach(function(album){
      app.data.server.tags = lodash.union(app.data.server.tags, album.tags);
    });
    
    app.data.libs = {
      lodash: lodash,
      moment: moment,
    };
    
    console.timeEnd('LOADING DATA');
    cb && cb({ok: true});
  });
};

var loadFeedData = function(url, cb){
  if(!app.data.meta.feedUrl){
    return cb(new Error('no facebook feed url specified'));
  }
  
  //console.log(querystring.stringify(app.data));
  //var url = app.data.meta.feedUrl.base + '?' + querystring.stringify(app.data.meta.feedUrl.query);
  //console.log(url);
  
  request(url, function (error, response, body) {
    if(error || response.statusCode !== 200){
      var msg = 'Facebook feed data can not be loaded';
      return cb && cb({ok: false, msg: error || msg});
    }
    
    var feed = JSON.parse(body).data;
    feed = feed.filter(function(ad){
      //if(ad.type !== 'photo') return;
      if(!ad.message || ad.message && (!lodash.includes(ad.message, '€') || false)) return;
      
      var message = ad.message.split('\n');
      ad.title = message.shift();
      var title = message.shift();
      ad.desc = message.join('\n');
      ad.desc = ad.desc && !ad.desc.length ? false : ad.desc;
      ad.img = ad.full_picture;
      ad.updated = ad.updated_time;
      ad.user = ad.from && ad.from.name;
      ad.userId = ad.from && ad.from.id;
      ad.groupName = lodash.includes(ad.id, '345684355546564') ? 'Skelbimai Telsuose' : 'Turgielis';
      
      if(title){
        title = title.split(' - ');
        ad.price = title.shift();
        ad.loc = title.pop();
      }
      return ad;
      
    });
    cb && cb({ok: true, feed: feed});
  });
};

// setting view engine to dot
app.engine('dot', engine.__express);
app.set('view engine', 'dot');

// setting public folder
app.use(express.static(__dirname + '/public'));

// loading data when setver starts
app.loadPoinoutData = function(cb){
  
  loadData(function(json){
    if(!json.ok) throw json.msg;

    var urls = {
      turgielis:  app.data.meta.feedUrl.base + '?' + querystring.stringify(app.data.meta.feedUrl.query),
      telsiu:     'https://graph.facebook.com/v2.6/345684355546564/feed' + '?' + querystring.stringify(app.data.meta.feedUrl.query),
    };
    
    var feed = [];
    async.each(urls, function(url, callback){
      loadFeedData(url, function(jsonFeed){
        if(!jsonFeed.ok) callback(new Error(jsonFeed.msg));
        feed = feed.concat(jsonFeed.feed);
        console.log('\nLOADED\n', url);
        callback();
      });
    }, function(err){
      if(err) throw err; 
      //app.data.feed = feed;
      api.loadFirebaseAds(function(ads){
        feed = feed.concat(ads);
        app.data.feed = lodash.sortBy(feed, 'updated').reverse();
        cb && cb({ok: true});  
      });
    });
  });
}

//auto loader
app.loadPoinoutData(function(){
  console.log('\nServer Started');
  app.listen(port, host);
});

setInterval(function(){
  app.loadPoinoutData();
}, 600000);

// data restart route
app.get('/refresh/:poinoutApp', function(req, res, next){
  res.set('Access-Control-Allow-Origin', '*');
  
  if(!req.params.poinoutApp || req.params.poinoutApp && req.params.poinoutApp !== poinoutApp){
    return res.json({ok: false, msg: 'Poinout app is not correct'});
  }
  
  app.loadPoinoutData(function(json){
    res.json(json);
  });
});

// ROUTING


app.get('/data', function(req, res){
  res.json(app.data);
});

app.get(['/app/*', '/', '/app/', '*'], function (req, res){
  var jsonData = {
    feed: app.data.feed,
    user: req.session && req.session.user
  }
  app.data.json = JSON.stringify(jsonData);
  app.data.user = req.session && req.session.user;
  res.render('main', app.data);
});

/*
app.get('/album/:id*', function (req, res) {
  var selectedAlbumIndex = lodash.findIndex(app.data.flickr, {id: req.params.id});
  
  app.data.server.prevAlbum = lodash.get(app.data.flickr, selectedAlbumIndex-1) 
    || lodash.get(app.data.flickr, app.data.flickr.length-1);
  
  app.data.server.selectedAlbum = lodash.get(app.data.flickr, selectedAlbumIndex);
  
  app.data.server.nextAlbum = lodash.get(app.data.flickr, selectedAlbumIndex+1) 
    || lodash.get(app.data.flickr, 0);
    
  app.data.server.url = req.get('host') + req.url;
    
  res.render('album', app.data);
});*/