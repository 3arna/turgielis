var request = require('request');
var querystring = require("querystring");
var async = require("async");

var facebookAuth = {
  
  opt : {
    clientId: '601575836607971',
    clientSecret: '40b6eb6239df05141ac21c717121f079',
    callbackUrl: "http://dev-c9-noneede.c9.io/auth/facebook/callback",
    mainCallbackUrl: "http://www.buy365sell.com/auth/facebook/callback",
    scope: 'email,photoURL,public_profile',
  },
  
  login : function(req,res,next){
    console.log(req.get('host'));
    var cb = req.get('host').indexOf('c9') != -1 ? facebookAuth.opt.callbackUrl : facebookAuth.opt.mainCallbackUrl;
    //email scope
    var code = req.query.code;
    var tokenUrl = 
    'https://graph.facebook.com/oauth/access_token' +
    '?client_id=' + facebookAuth.opt.clientId +
    '&redirect_uri=' + cb +
    '&client_secret=' + facebookAuth.opt.clientSecret +
    '&code=' +code;
    
    async.waterfall([
      
        function(callback){
          request(tokenUrl, function(err, body){
            var token = body.body ? querystring.parse(body.body).access_token : false;
            callback(null,  token);
          });
        },
        
        function(token , callback){
          var getInfoUrl =
          'https://graph.facebook.com/v2.0/me'+
          '?access_token='+token;
          request(getInfoUrl, function(err, body){
            callback(null, JSON.parse(body.body), token);
          });
        }
        
    ], function (err, profile, token) {
      profile.type = 'fb';
      profile.token = token;
      req.profile = profile;
      next();
      
    });
  },
  
  redirect : function(req,res,next){
    var cb = req.get('host').indexOf('c9') != -1 ? facebookAuth.opt.callbackUrl : facebookAuth.opt.mainCallbackUrl;
    var url = 
    'https://www.facebook.com/dialog/oauth'+
    '?client_id='+facebookAuth.opt.clientId+
    '&redirect_uri='+cb+
    '&scope='+facebookAuth.opt.scope;
    res.redirect(url);
  },
  
  logout : function(req,res){
    var path = req.session.user.page;
    delete req.session.user;
    req.session.user = {
        'loged' : false,
        'class' : 'guest'
      }
    req.session.user.page = req.originalUrl;
    res.redirect(path);
  }
  
}

module.exports = facebookAuth;