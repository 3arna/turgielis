var firebase = require("firebase");
var bodyParser = require('body-parser');
var lodash = require('lodash');
var moment = require('moment');
var facebookAuth = require('./libs/fb.js');

facebookAuth.opt = {
  clientId: '213800112367036',
  clientSecret: 'e87a9e95b8a3088e233187dbdf78a2d9',
  callbackUrl: 'http://stattrack-c9-noneede.c9.io/auth/facebook/callback',
  mainCallbackUrl: 'http://www.turgielis.lt/auth/facebook/callback',
  scope: 'email'
}

firebase.initializeApp({
  apiKey: 'AIzaSyBRpIVYpxn5b-4CGG5V9Lk4swidYNdQAqk',
  authDomain: "turgielis-8e7ae.firebaseapp.com",
  databaseURL: 'https://turgielis-8e7ae.firebaseio.com',
  storageBucket: 'turgielis-8e7ae.appspot.com',
});

var db = firebase.database();

exports.load = function(app) {
  
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
  
  app.get('/test', function(req, res, next){
    res.send('test works');
  });
  
  app.post('/api/ad', function(req, res){
    if(!req.body) return res.json({ok: false, err: 'data is incorrect'});
    if(!req.session || !req.session.user) return res.json({ok: null, err: 'unauthenticated user can not post ad'});
    
    var newPostKey = db.ref().child('ads').push().key;
    var formData = {
      user: req.session.user.name || 'Bevardis',
      userId: req.session.user.id || null,
      price: !!req.body.price && ('€' + req.body.price) || 'Veltui',
      img: req.body.img || null,
      desc: req.body.desc || 'Be aprasymo',
      title: req.body.title || 'Be pavadinimo',
      loc: req.body.loc || 'Telsiai',
      updated: moment().format(),
      created: moment().format(),
      provider: 'direct',
      ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    }
    
    /*app.data.feed = [formData].concat(app.data.feed);
    res.json({ok: true, data: app.data.feed});*/
    db.ref('ads/' + newPostKey).update(formData);
    
    app.loadPoinoutData(function(){
      res.json({ok: true, data: app.data.feed});
    });
  });
  
  app.post('/api/remove', function(req, res){
    if(!req.body) return res.json({ok: false, err: 'data is incorrect'});
    if(!req.session || !req.session.user) return res.json({ok: null, err: 'unauthenticated user can not remove ad'});
    
    db.ref('ads/' + req.body.id).remove();
    app.loadPoinoutData(function(){
      res.json({ok: true, data: app.data.feed});
    });
  });
  
  app.get('/api/feed', function(req, res){
    res.json({ok: true, data: app.data.feed});
  });
  
  /*app.get('/api/feed', function(req, res){
    res.json(app.data.feed);
  });*/
  
  app.get('/api/data', function(req, res){
    res.json(app.data);
  });
  
  app.get('/login', facebookAuth.redirect);
  app.get('/auth/facebook/callback', facebookAuth.login, function(req, res){
    
    /*firebase.auth().onAuthStateChanged(function(firebaseUser) {
      var user = firebase.auth().currentUser;
      if(!!firebaseUser){
        console.log(req.session);
        req.session.user = {
          uid: firebaseUser.uid,
          displayName: firebaseUser.displayName,
          photoURL: firebaseUser.photoURL,
          email: firebaseUser.email,
          providerData: firebaseUser.providerData
        }
        res.redirect('/');
      }
    });*/
    
    var credential = firebase.auth.FacebookAuthProvider.credential(req.profile.token);
    firebase.auth().signInWithCredential(credential).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
    });
    
    req.session.user = {
      name: req.profile.name, 
      id: req.profile.id,
    };
    
    res.redirect('/');
  });
  
  app.get('/logout', function(req,res){
    if(req.session && req.session.user){
      delete req.session.user;
    }
    res.json({ok: true});
  });
}

exports.loadFirebaseAds = function(cb){
  db.ref('/ads/').once('value').then(function(snapshot){
    var adsArr = [];
    var obj;
    for(var i in snapshot.val()){
      obj = snapshot.val()[i];
      obj.id = i;
      if(obj.img && lodash.includes(obj.img, 'imgur')){
        var imgUrl = obj.img.split('.');
        imgUrl[2] += 'm';
        obj.img = imgUrl.join('.');
      }
      adsArr.push(obj)
    }
    //app.data.feed = lodash.union(adsArr, app.data.feed);
    //app.data.feed = lodash.sortBy(app.data.feed, 'updated').reverse();
    //_.sortBy(array, function(value) {return value + ''; });
    cb(adsArr);
  });
}

/*app.get('/album/:id*', function (req, res) {
  var selectedAlbumIndex = lodash.findIndex(app.data.flickr, {id: req.params.id});
  
  app.data.server.prevAlbum = lodash.get(app.data.flickr, selectedAlbumIndex-1) 
    || lodash.get(app.data.flickr, app.data.flickr.length-1);
  
  app.data.server.selectedAlbum = lodash.get(app.data.flickr, selectedAlbumIndex);
  
  app.data.server.nextAlbum = lodash.get(app.data.flickr, selectedAlbumIndex+1) 
    || lodash.get(app.data.flickr, 0);
    
  app.data.server.url = req.get('host') + req.url;
    
  res.render('album', app.data);
});*/